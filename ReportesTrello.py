# This code sample uses the 'requests' library:
# http://docs.python-requests.org
import requests
import json
import pprint

key=''
token=''


def obtener_nombre_tablero(json_data):
   return json_data["name"]

def obtener_miembros(json_data):
   miembros = json_data["memberships"]


def obtener_tarjetas_lista(list_id):
   url = f"https://api.trello.com/1/lists/{list_id}/cards"
   print(url)
   response = requests.request(
      "GET",
      url,
      params={"key":key,"token":token}
   )

   return response.json()

def obtener_listas(board_id):
   url = f"https://api.trello.com/1/boards/{board_id}/lists"
   print(url)
   response = requests.request(
      "GET",
      url,
      params={"key":key,"token":token}
   )

   return response.json()   

def imprimir_tarjetas(tarjetas):
   for t in tarjetas:
      print(t)
      print("\n")

url = "https://api.trello.com/1/members/me/boards"

headers = {
   "Accept": "application/json"
}
'''response = requests.request(
   "GET",
   url
)'''

response = requests.request('GET',url,headers=headers,params={'key':key,'token':token})

#pprint.pprint(response.text)
#json = json.loads(response.text)#, sort_keys=True, indent=4, separators=(",", ": ")
json=response.json()

tablero_uno = json[1]
#tarjetas = obtener_tarjetas(tablero_uno["id"])
listas = obtener_listas(tablero_uno["id"])

for lista in listas:
   tarjetas = obtener_tarjetas_lista(lista["id"])
   imprimir_tarjetas (tarjetas)
   print( "\n")
   print ("<================>")
